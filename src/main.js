import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';

import Index from './Index.vue';

import Convocatorias from './convocatoria/Convocatorias.vue';
import NuevaConvocatoria from './convocatoria/NuevaConvocatoria.vue';

import ConfigurarCriterios from './convocatoria/ConfigurarCriterios.vue';
import ConfigurarRelevancia from './convocatoria/ConfigurarRelevancia.vue';
import ModificarSeleccion from './convocatoria/ModificarSeleccion.vue';
import Reclutados from './convocatoria/Reclutados.vue';
import store from './vuex-store.js';

require("./style.scss");
import App from './App.vue';
Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Vuex);
Vue.mixin({
  data: function() {
    return {
      get getConstants() {
        return {
                convocatoriaSteps:[{step:"Nueva convocatoria",link:"#/convocatoria/nueva"},
                                   {step:"Criterios de selección",link:"#/convocatoria/nueva-criterios"},
                                   {step:"Relevancia de criterios",link:"#/convocatoria/nueva-relevancia"}],
                colors:["#14837E","#DFDFDF","#E0E590","#24928D","#BCCB38","#3CA09C","#95A32E","#63ACAD","#73812F","#006763"],
                routes:{ convocatorias: "/convocatorias",
                         nuevaConvocatoria: "/convocatoria/nueva",
                         nuevaConfigurarCriterios: "/convocatoria/nueva-criterios",
                         nuevaConfigurarRelevancia: "/convocatoria/nueva-relevancia",
                         modificarResultados: "/convocatoria/modificar-resultados",
                         reclutados: "/convocatoria/nueva-reclutados"

                       },
                API_URL:process.env.API_URL
              };
      },
      //currentCriteriaData:"55",
      //currentCriteriaRelevance:null,
    }

  }
})
const routes = [
  //{ path: '/index', alias: '/', component: Index},
  { path: '/convocatorias', alias: '/', component: Convocatorias},
  {path: '/convocatoria/nueva', component: NuevaConvocatoria},
  {path: '/convocatoria/nueva-criterios', component: ConfigurarCriterios},
  {path: '/convocatoria/nueva-relevancia', component: ConfigurarRelevancia},
  {path: '/convocatoria/nueva-reclutados', component: Reclutados},
  {path: '/convocatoria/modificar-resultados', component: ModificarSeleccion},
]

// Create the router instance and pass the `routes` option
const router = new VueRouter({
  routes
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
