import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    currentCriteriaData:null,
    currentCriteriaRelevance:null,

    rankingData:null,
    formData:{nombre:'',nVoluntarios:'',descripcion:'',plantilla:''},
    //backendUri:process.env.API_URL
  }
  /*,
  actions: {
    setMissionForm({commit}, form){
      commit('SET_MISSION_FORM', form);
    }
  },
  mutations: {
    SET_MISSION_FORM: (state, { form }) => {
      state.missionForm = form;
    }
  },
  getters: {
    openMissionForm: state => state.missionForm
  }*/
})
export default store
