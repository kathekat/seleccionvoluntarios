
# Waynakay Frontend #

Proyecto de Selección masiva de voluntarios desarrollado por Citiaps.
## Instalación
El proceso de instalación del frontend de Waynakay se lista a continuación:
* Instalar [Node](https://nodejs.org/es/download/) (versión 6.8 mínimo).
* ejecutar `npm install`en la carpeta actual.
* Instalar servicio Rest momentáneo `sudo npm install -g json-server`.
* Ejecutar servicio Rest `json-server --watch db/database.json`.
* Ejecutar en otra consola `npm run dev`.
