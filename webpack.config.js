const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BabiliPlugin = require("babili-webpack-plugin");
const webpack = require("webpack");
//const BabiliPlugin = require("babili-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = (options = {}) => ({
  entry: './src/main.js',
  output: {
    filename: 'scripts.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
        rules: [
          {
            test: /\.vue$/,
            use:[{loader:"vue-loader"}]
          },
          {
            test: /\.scss$/,
            use: [{
                  loader: "style-loader" // creates style nodes from JS strings
              }, {
                  loader: "css-loader" // translates CSS into CommonJS
              }, {
                  loader: "sass-loader" // compiles Sass to CSS
              }]
          },
          {
            test: /\.css$/,
            use: [{
                  loader: "style-loader" // creates style nodes from JS strings
              }, {
                  loader: "css-loader" // translates CSS into CommonJS
              }]
          },
          {
            test:  /\.(jpeg|gif|png|svg|woff|ttf|wav|mp3)$/,
            loader: "file-loader?name=img/img-[hash:6].[ext]"
          }
        ]
    },
    plugins:options.dev?[
      new HtmlWebpackPlugin({
        template: 'src/index.html'
      }),
      new webpack.DefinePlugin({
        'process.env':{
          'NODE_ENV': JSON.stringify('development'),
          //'API_URL': JSON.stringify('http://localhost:3000/')
          'API_URL': JSON.stringify('http://localhost:8082/api/v1/')
        }
      })
    ]:[
      new HtmlWebpackPlugin({
        template: 'src/index.html'
      }),
			new BabiliPlugin(),
      new CopyWebpackPlugin([
        {
          from: 'src/img', to: 'src/img'
        },
        {
          from: 'db', to: 'db'
        }
      ]),
      new webpack.DefinePlugin({
        'process.env':{
          'NODE_ENV': JSON.stringify('production'),
          'API_URL': JSON.stringify('http://localhost:3000/')
        }
      })
    ],
      devtool: options.dev ? '#eval-source-map' : '#source-map'
})
